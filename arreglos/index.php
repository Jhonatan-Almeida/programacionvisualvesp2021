<?php

//El arreglo es un conjunto de datos utilizables como uno sola variable , contenedor de datos

// $arreglo = array(elementos);
// $zapatos = [elementos];

 //                     0        1        2       3
$zapatillas = array("nike","rebook","adidas","jordan");

var_dump($zapatillas);

echo "<br>";

print_r($zapatillas);

echo "<br>";

echo $zapatillas[0]."<br>";

//foreach ($zapatillas as $key => $value) {
//    
//} se utiliza con arreglos con identificador 

echo "<ul>";
foreach ($zapatillas as $zapatilla) {
    echo "<li>".$zapatilla."<br>";
}
echo "</ul>";


echo "<h1>Arreglo con Identificación</h1>";

$personas =[
    array(
        'nombre'=>'Andy Joel',
        'apellidos'=>'Plua Pincay',
        'edad'=>21,
        'direccion'=>'Paraiso de la Flor'
        ),
    array(
        'nombre'=>'Freddy Ismael',
        'apellidos'=>'Pluas Pincay',
        'edad'=>19,
        'direccion'=>'Sergio Toral'
        ),
    array(
        'nombre'=>'Justin Joel',
        'apellidos'=>'Saltos Peña',
        'edad'=>19,
        'direccion'=>'Santa Lucia'
        ),
    array(
        'nombre'=>'Jose Leonardo',
        'apellidos'=>'Davila Rosales',
        'edad'=>21,
        'direccion'=>'Machala'
        ),
];

for($i=0;$i<count($personas);$i++){
    echo "Mi nombre es :".$personas[$i]['nombre']." ".$personas[$i]['apellidos']
            ." mi edad es: ".$personas[$i]['edad']." la direccion es: ".$personas[$i]['direccion']."<br>";
}

$tabla = "<table border='2'>
            <tr>
                <td>Nombres</td>
                <td>Edad</td>
                <td>Direccion</td>
            </tr>";
foreach ($personas as $key => $persona) {
    $tabla.=  "<tr>
                <td>".$persona['nombre']." ".$persona['apellidos']."</td>
                <td>".$persona['edad']."</td>
                <td>".$persona['direccion']."</td>
            </tr>";
}
$tabla .= "</table>";

echo $tabla;


//<tr>
//                <td>".$personas[1]['nombre']['nombre1']." ".$personas[1]['nombre']['nombre2']." ".$personas[1]['apellidos']."</td>
//                <td>".$personas[1]['edad']."</td>
//                <td>".$personas[1]['direccion']."</td>
//            </tr>
//            <tr>
//                <td>".$personas[2]['nombre']." ".$personas[0]['apellidos']."</td>
//                <td>".$personas[2]['edad']."</td>
//                <td>".$personas[2]['direccion']."</td>
//            </tr>