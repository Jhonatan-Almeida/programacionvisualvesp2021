<?php
// if else
//El constructor if es una de las características más importantes de muchos lenguajes, 
//incluido PHP. Permite la ejecución condicional
//de fragmentos de código. PHP dispone de una estructura if que es similar a la de C:
//if (condicion){
//    codigo a ejecutarse si la condición es correcta 
//}else{
//    si la condiciones es incorrecta
//}
// if (condicion):
//    codigo a ejecutarse si la condición es correcta 
//else:
//    si la condiciones es incorrecta
//endif
// funciones internas 
// isset() .- si existe o no una variable
// empty() .- si una variable esta vacia
// is_null() .- nos dice si una variable es nula 

// Operadores de igualdad
// == .- si es igual a una variable
// === .- si es igual que la viariable y el tipo de variable 
// != .- no dice si es diferente a la variable 
// !== .- nos dice que es diferente que la variable y su tipo 
// >= .- mayor igual que una variable 
// <= .- menor igual que una variable 
// <> .- diferente 

// Operadores lógicos 
// &&.- and 
// || .- or 

echo "<h1>COndicion IF</h1>";

$edad = $_GET['edad'];
 
if (empty($edad)){
    echo "El componente esta vacio";
}else{
    if($edad >= 18):
            echo "La persona es mayor de edad";
        else:
            echo "La persona es menor de edad";
    endif;
}
 echo "<br>";
echo "<h1>Condicion IF else</h1>";

$color= $_GET['color'];
if (empty($color)){
    echo "El componente esta vacio";
}else{
        if($color == 1){
                echo "el color es azul";
        }else if($color == 2){
                echo "el color es verde";
        }else if($color == 3){
                echo "el color es amarillo";
        }else if($color == 4){
                echo "el color es rojo";
        }else{
            echo "No tiene color";
        }
    }   

echo "<br>";
echo "<h1>Condicion Switch-case</h1>";

switch ($color){
    case 1:
        echo "el color es gris";
        break;
    case 2:
        echo "el color es rosado";
        break;
    case 3:
        echo "el color es cafe";
        break;
    case 4:
        echo "el color es naranja";
        break;
}