<?php
// iniciamos la sessión
session_start();
if(isset($_POST)){
    
    // obtenemos la conexion
    require_once './includes/conexion.php';
    // obtenemos los parametros del fomulario
    $titulo = isset($_POST['titulo']) ?  mysqli_real_escape_string($db,$_POST['titulo']): false;
    $descripcion = isset($_POST['descripcion']) ?  mysqli_real_escape_string($db,$_POST['descripcion']): false;
    $categoria = isset($_POST['categoria']) ? (int)$_POST['categoria']: false;
    $usuario = $_SESSION['usuario']['id'];  
    // validar
     $errores = array();

    if(!empty($titulo)){
        $titulo_valido = true;
    } else{
        $titulo_valido = false;
        $errores['titulo']='El titulo de la entrada no es valido';
    }
    if(!empty($descripcion)){
        $descripción_valida = true;
    } else{
        $descripción_valida = false;
        $errores['descripcion']='La descripción de la entrada no es valida';
    }
    
    if (count($errores)== 0){
        // guardar la información
        $sql = "insert into entradas values (null,$usuario,$categoria,'$titulo','$descripcion',CURDATE());";
        $guardar = mysqli_query($db, $sql);
        if($guardar){
            $_SESSION['correcto'] = 'La entrada fue guardada con exito';
        }else{
             $_SESSION['errores_entrada']= 'Existe un error al guardar';
        }
        
    }else{
        $_SESSION['errores']= $errores;
        
    }
}
header("Location: ./crear-entrada.php");