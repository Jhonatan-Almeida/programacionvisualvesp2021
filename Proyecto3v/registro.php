<?php

$errores = array();
if(isset($_POST)){
    //Incluimos la conexión 
    include './includes/conexion.php';
     // rescatar los elementos del formulario 
     if (!isset($_SESSION)){
         session_start();
     }
     
     //echo ($_POST['nombre']);die();
    
    $nombre = isset($_POST['nombre']) ? mysqli_real_escape_string($db,$_POST['nombre']): false;
    $apellidos = isset($_POST['apellidos']) ? mysqli_real_escape_string($db,$_POST['apellidos']): false;
    $email = isset($_POST['email']) ? mysqli_real_escape_string($db,$_POST['email']): false;
    $password = isset($_POST['password']) ? mysqli_real_escape_string($db,$_POST['password']): false;
    
    //Validar campos 
    if (!empty($nombre) && !is_numeric($nombre) && !preg_match("/[0-9]/", $nombre)){
        $nombre_valido = true;
    }else{
        $nombre_valido = false;
        $errores['nombre'] = "El nombre no es valido";//$errores['nombre']
    }
    if (!empty($apellidos) && !is_numeric($apellidos) && !preg_match("/[0-9]/", $apellidos)){
        $apellidos_valido = true;
    }else{
        $apellidos_valido = false;
        $errores['apellido'] = "El apellido no es valido";
    }
    if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)){
        $email_valido = true;
    }else{
        $email_valido = false;
        $errores['email'] = "El correo no tiene el formato";
    }
    if (!empty($password)){
        $password_valido = true;
    }else{
        $password_valido = false;
        $errores['password'] = "La contraseña esta vacia";
    }
    
    $guardar_usuario = false;
    
    if(count($errores) == 0 ){
        $guardar_usuario = true;
         // cifrar la contraseña 
        // md5($strin5); sha($string)
        $password_segura = password_hash($password, PASSWORD_BCRYPT, ['cost'=>4]);
        
        // insertar en la base de datos 
        $sql = "insert into usuarios values(null, '$nombre', '$apellidos', '$email','$password_segura', CURDATE())";
        //echo $sql;die();
        $guardar = mysqli_query($db, $sql) ;
        
        if ($guardar){
            $_SESSION['completado'] = "El registro se guardo con exito";
        }else{
            $_SESSION['errores']['general'] = "Existe un error en el registro";
        }
    }else{
        $_SESSION['errores'] = $errores;
    }
}
header("Location: index.php");

