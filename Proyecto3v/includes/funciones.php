<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function mostrarError($errores,$campo){
    $alerta = "";
    if (isset($errores[$campo]) && !empty($errores[$campo])){
        $alerta = "<div class='alerta alerta-error'>".$errores[$campo]."</div>";
    }
    return $alerta;
}
function borrarErrores(){
    $borrado = false;
    if(isset($_SESSION['errores']['general'])){
        $_SESSION['errores']['general'] = null;
        $borrado = true;
    }
    if(isset($_SESSION['completado'])){
        $_SESSION['completado'] = null;
        $borrado = true;
    }
    if(isset($_SESSION['errores'])){
        $_SESSION['errores'] = null;
        $borrado = true;
    }
    if(isset($_SESSION['error_login'])){
        $_SESSION['error_login'] = null;
        $borrado = true;
    }
    if(isset($_SESSION['completo_categoria'])){
        $_SESSION['completo_categoria'] = null;
        $borrado = true;
    }
    if(isset($_SESSION['error_categoria'])){
        $_SESSION['error_categoria'] = null;
        $borrado = true;
    }
    if(isset($_SESSION['correcto'])){
        $_SESSION['correcto'] = null;
        $borrado = true;
    }
    if(isset($_SESSION['errores_entrada'])){
        $_SESSION['errores_entrada'] = null;
        $borrado = true;
    }
}
function conseguirCategorias($conexion){
    $sql = "select * from categorias order by id";
    $categorias = mysqli_query($conexion, $sql);
    
    $resultado = array();
    if ($categorias && mysqli_num_rows($categorias) >= 1){
        $resultado = $categorias;
    }
    return $resultado;
}
function conseguirCategoria($conexion, $id){
    $sql = "select * from categorias where id = $id";
    $categorias = mysqli_query($conexion, $sql);
    
    $resultado = array();
    if ($categorias && mysqli_num_rows($categorias) >= 1){
        $resultado = mysqli_fetch_assoc($categorias);
    }
    return $resultado;
}

function conseguirEntradas($conexion, $limite = null, $categoria = null){
    $sql = "select e.*, c.nombre from entradas e
                inner join categorias c on c.id = e.categoria_id order by e.id desc";
    
    if($limite){
        $sql .= " limit 3";
    }
    $entradas = mysqli_query($conexion, $sql);
    $resultado = array();
    if ($entradas && mysqli_num_rows($entradas) >=1 ){
        $resultado = $entradas;
    }
    return $resultado;
}

function conseguirEntrada($conexion, $id){
    $sql = "select e.*, c.nombre as categoria, 	CONCAT(u.nombre,' ',u.apellidos) as usuario from entradas e
            inner join categorias c on e.categoria_id = c.id
            inner join usuarios u on e.usuario_id = u.id
            where  e.id = $id";
    $entradas = mysqli_query($conexion, $sql);
    $resultado = array();
    if ($entradas && mysqli_num_rows($entradas) >=1 ){
        $resultado = mysqli_fetch_assoc($entradas);
    }
    return $resultado;
}

