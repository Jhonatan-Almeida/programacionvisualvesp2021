<?php
    include './includes/cabecera.php';
    include './includes/lateral.php';
?>
            
            <div id="principal">
                <h1>Entradas del Blog</h1>
                <?php 
                 
                 $entradas = conseguirEntradas($db, true);
                 if (!empty($entradas)):
                     while($entrada = mysqli_fetch_assoc($entradas)):
                ?>
<!--                  Aqui va ir todos los articulos de las entrdas mientras itere el while-->
                    <article class='entrada'>
                        <a href="entrada.php?id=<?= $entrada['id']?>">
                            <h2><?= $entrada['titulo']?></h2> 
                            <span class="fecha"><?=$entrada['nombre'].' | '.$entrada['fecha']?></span>
                            <p><?= substr($entrada['descripcion'],0,180)."...Leer mas</a>"; ?></p>
                        </a>

                    </article>
                <?php 
                     endwhile;
                 endif;
                ?>
            </div>
 
<?php
    include './includes/pie.php';
?>
    