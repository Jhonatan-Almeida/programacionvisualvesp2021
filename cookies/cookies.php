<?php

/* 
 Cookie: Es un fichero que se almacena en el ordenador del usuario que visita
 * la web, con el fin de recordar datos o ratrear el comportamiendo del mismo
 * en la web.
 */
// Crear cookie
// setcookie("nombre", "valor que solo puede ser texto", caducidad, ruta, dominio);

// cookie Básica

setcookie('galleta', 'Valor de la galleta');

// cookie con caducidad

setcookie('oneyaer', 'Valor de galleta con caducidad',  time()+(60*60*24*365));

?>
 
<a href="ver_cookie.php">Ver cookies</a>