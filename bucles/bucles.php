<?php

/* 
 Un bucle o ciclo, en programación, es una secuencia de instrucciones de código que se ejecuta repetidas veces, 
 * hasta que la condición asignada a dicho bucle deja de cumplirse.
 * Los tres bucles más utilizados en programación son el bucle while, el bucle for y el bucle do-while.*/
/*while(codicion){
    Secuencia de instrucciones dadas;
}
 * 
 for($i=0;$i<=10;$i++){
 *      Secuencia de intrucciones dadas;
 * }
 * 
 *  * INCREMENTALES - DECREMENTALES
 * $num ++; $num=$num +1; $num+1;
 * $num --; $num=$num -1; $Num-1; 
 * 
 * /*
 * do{
 *     Secuencia de intrucciones dadas;
 * }while(condición)
 */

//$num = $_GET['num'];
//if (isset($num)){
//    echo "<h1>Tabla de multiplicar del $num </h1><br>";
//    $inc = 0;
//    while($inc <= 10){
//        echo "$inc x $num = ".$inc*$num."<br>";
//        $inc++;
//    }
//}else{
//    echo "Ingrese la variable en la URL";
//}


//$num = $_GET['num'];
//if (isset($num)){
//    echo "<h1>Tabla de multiplicar del $num con FOR</h1><br>";
//    for($inc=0;$inc<=10;$inc++){
//        echo "$inc x $num = ".$inc*$num."<br>";
//    }
//}else{
//    echo "Ingrese la variable en la URL";
//}


$num = $_GET['num'];
if (isset($num)){
    echo "<h1>Tabla de multiplicar del $num con DO-WHILE </h1><br>";
    $inc = 0;
    do{
        echo "$inc x $num = ".$inc*$num."<br>";
        $inc++;
    }while($inc <= 10);
}else{
    echo "Ingrese la variable en la URL";
}